 <!DOCTYPE html>
  <html>
    <head>
      <meta charset="UTF-8">
      <meta name="description" content="Descripcion...">
      <meta name="keywords" content="HTML,CSS,XML,JavaScript">
      <meta name="author" content="John Doe">
      <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=0,maximum-scale=1,user-scalable=no">
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
      <link type="text/css" rel="stylesheet" href="css/jqueryFullpage.min.css"  media="screen,projection"/>
        <link type="text/css" href="css/style.css" rel="stylesheet">
      <link type="text/css" rel="stylesheet" href="css/vectorial.css"  media="screen,projection"/>
      <link type="text/css" rel="stylesheet" href="css/interna.css"  media="screen,projection"/>
      <!--
      <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.1.20/jquery.fancybox.min.css"> -->
    </head>
    <body>


    <header>
        <nav class="cd-stretchy-nav">
            <a class="cd-nav-trigger" href="#0">
                Menu
                <span aria-hidden="true"></span>
            </a>

            <ul>
                <li><a href="fondo.php" class="active"><span>¿Qué es el Fondo?</span></a></li>
                <li><a href="convocatoria.php"><span>Convocatoria 2017</span></a></li>
                <li><a href="proyecto.php"><span>Postular mi proyecto</span></a></li>
                <li><a href="preguntas.php"><span>Preguntas frecuentes</span></a></li>
                <li><a href="contacto.php"><span>Contacto</span></a></li>
            </ul>

            <span aria-hidden="true" class="stretchy-nav-bg"></span>
        </nav>
    </header>

    <!-- Contenido -->
    <style>
        .flex.fullCenter.contentSectionBanner{
            background: url(img/Que-es-el-fondo-v2.jpg) center;
            background-size: cover;
        }
    </style>

    <div class="flex fullCenter menuContenedor">
        <img src="img/logo.png" alt="fondo fundación WWWB Colombia">
        <div class="flex menuItems">
            <ul class="flex menu">
                <li class="active">
                    <a href="fondo.php">¿Qué es el Fondo?</a>
                </li>
                <li>
                    <a href="convocatoria.php">Convocatoria 2017</a>
                </li>
                <li>
                    <a href="proyecto.php">Postular mi proyecto</a>
                </li>
                <li>
                    <a href="preguntas.php">Preguntas frecuentes</a>
                </li>
                <li>
                    <a href="contacto.php">Contacto</a>
                </li>
            </ul>
            <div class="flex fullCenter redesSociales">
                <a href="#">
                    <i class="fa fa-facebook" aria-hidden="true"></i>
                </a>
                <a href="#">
                    <i class="fa fa-twitter" aria-hidden="true"></i>
                </a>
                <a href="#">
                    <i class="fa fa-youtube-play" aria-hidden="true"></i>
                </a>
            </div>
        </div>
    </div>

   
  
  <div id="fullpage">

      <div class="section " id="section0">
          <div class="flex fullCenter contentSectionBanner">
              <div class="mensaje-banner">
                <h2>¿Qué es<br><b>el Fondo?</b></h2>
                <p>"Contexto para el emprendimiento de las mujeres en ámbitos rurales"</p>
              </div>
          </div>
            <a data-menuanchor="secondPage" href="#secondPage" class="flecha">
                <i class="fa fa-angle-down" aria-hidden="true"></i>
            </a>
      </div>

      <div class="section" id="section1">
          <div class="flex content-col height-100">
            <div class="flex col2 margin-left-80">
                <h2>¿A quién <b>está dirigido?</b></h2>
                <h1>Está dirigido a <b>estudiantes de maestría o doctorado (colombianos y extranjeros)</b> que tengan un proyecto de 
                    investigación de grado aprobado. También pueden participar <b>grupos de investigación</b> interesados en desarrollar 
                    proyectos de investigación, innovación social y desarrollo tecnológico en el campo de investigación dispuesto 
                    para cada convocatoria y que incluya al Valle del Cauca como sitio de trabajo de campo. </h1>
            </div>
            <div class="col1 align-center">
                <img class="margin-left-80 max-width-700" src="img/Que-es-el-fondo2.jpg">
            </div>
            <a class="centrado" data-menuanchor="4thpage" href="#4thpage">
                <i class="fa fa-angle-down flechaArrow" aria-hidden="true"></i>
            </a>
          </div>
      </div>

      <div class="section gris" id="section3">
            <div class="flex content-col-rever">
                <div class="col1 margin-left-80 orderResponsive-2">
                    <img src="img/Que-es-el-fondo3.png" class="height-86">
                </div>
              <div class="col2 margin-left-80">
                  <h2>¿Donde <b>aplica?</b></h2>
                  <p><b>Para la primera versión de esta convocatoria</b> del Fondo Fundación WWB Colombia para la Investigación, 
                      se plantea como objetivo: promover investigaciones que se propongan estudiar las dinámicas, tensiones, 
                      imaginarios, relaciones, desafíos, conflictos, y posibilidades actuales en espacios rurales con relación 
                      al emprendimiento liderado por o con participación de mujeres en el Valle del Cauca, Colombia.</p>                  
              </div>
            </div>
            <a class="centrado" data-menuanchor="4thpage" href="#4thpage">
                <i class="fa fa-angle-down flechaArrow" aria-hidden="true"></i>
            </a>
        </div>

        <div class="section" id="section3">
            <div class="flex content-col-rever only-text">
                <div class="margin-left-220">
                    <h2>Conozca a la <b>Fundación<br>WWB Colombia</b></h2>
                    <p><b>Para la primera versión de esta convocatoria</b> del Fondo Fundación WWB Colombia para la Investigación, 
                        se plantea como objetivo: promover investigaciones que se propongan estudiar las dinámicas, tensiones, 
                        imaginarios, relaciones, desafíos, conflictos, y posibilidades actuales en espacios rurales con relación 
                        al emprendimiento liderado por o con participación de mujeres en el Valle del Cauca, Colombia.</p>
                    <a class="btn">Visite el sitio de la Fundación WWB Colombia para conocer más</a>            
                </div>
            </div>
            <a class="centrado" data-menuanchor="4thpage" href="#4thpage">
                <i class="fa fa-angle-down flechaArrow" aria-hidden="true"></i>
            </a>
        </div>


        <div class="section" id="section1">
            <div class="flex content-col height-100">
                <div class="flex col2 margin-left-80">
                    <h2>Preguntas <b>frecuentes</b></h2>
                    <h1 class="max-width-auto">Consulte el espacio de preguntas frecuetes Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsam facil
                        is, ab eos harum dolore numquam eveniet repellat praesentium recusandae quod quam vel nisi 
                        esse reiciendis placeat neque explicabo. Blanditiis, omnis.</h1>
                    <a class="btn">Amplíe información aquí</a>
                </div>
                <div class="col1 align-center">
                    <img class="margin-left-80 max-width-700" src="img/Que-es-el-fondo4.png">
                </div>
                <a class="centrado" data-menuanchor="4thpage" href="#4thpage">
                    <i class="fa fa-angle-down flechaArrow" aria-hidden="true"></i>
                </a>
            </div>
        </div>



      <div class="section fp-auto-height azul" id="section2">
        <footer>
            <div class="flex contentFooter">
                <div class="col2">
                    <h3>Fondo Fundación WWB Colombia para la Investigación</h3>
                    <div class="flex menuFooter">
                        <div class="col1">
                            <ul>
                                <li>¿Qué es el Fondo?</li>
                                <li>¿Por qué lo hacemos?</li>
                                <li>¿A quién está dirigido?</li>
                                <li>¿Dónde aplica?</li>
                            </ul>
                        </div>
                        <div class="col1">
                            <ul>
                                <li>Postular mi proyecto</li>
                                <li>Financiamiento</li>
                                <li>Términos y condiciones</li>
                            </ul>
                        </div>
                        <div class="col1">
                            <ul>
                                <li>Preguntas frecuentes</li>
                                <li>Contáctenos</li>
                                <li>Conozca a la Fundación WWB Colombia</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col1">
                    <img src="img/logo.png">
                </div>
            </div>
            <div class="vectorial azulDestacado">
                <img src="img/vectorial.png">
            </div>
            <a class="anclaFoot" data-menuanchor="firstPage" href="#firstPage">
                <i class="fa fa-angle-up flechaArrow" aria-hidden="true"></i>
            </a>
        </footer>
      </div>

  </div>
  
  <div id="infoMenu">
      
  
  
  
  </div>
        

    <!-- /Contenido -->
    <!--Import jQuery before materialize.js-->
    <script
    src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
                                                      crossorigin="anonymous"></script>
    <script type="text/javascript" src="js/jquery.fullPage.js"></script>
    <script type="text/javascript" src="js/jquery.cycle.all.js"></script>
    <script type="text/javascript" src="https://cdn.carbonads.com/carbon.js?zoneid=1673&serve=C6AILKT&placement=codyhouseco"></script>
    <script src="https://codyhouse.co/demo/stretchy-navigation/js/main.js"></script>
    <!--
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.1.20/jquery.fancybox.min.js"></script> -->
    <script>
        $(document).ready(function(){
            
            $('#fullpage').fullpage({
              anchors: ['firstPage', 'secondPage', '3rdPage', '4thpage', 'lastPage'],
              menu: '#menu',
              'navigation': true,
			  'navigationPosition': 'right',
              responsiveWidth: 1200
            });
            $('a.ancla').click(function(e){
              e.preventDefault();
              var strAncla=$(this).attr('href');
                $('body,html').stop(true,true).animate({
                  scrollTop: $(strAncla).offset().top
                },1000);

            });
        });
      </script>
    </body>
  </html>
