 <!DOCTYPE html>
  <html>
    <head>
      <meta charset="UTF-8">
      <meta name="description" content="Descripcion...">
      <meta name="keywords" content="HTML,CSS,XML,JavaScript">
      <meta name="author" content="John Doe">
      <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=0,maximum-scale=1,user-scalable=no">
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
      <link type="text/css" rel="stylesheet" href="css/jqueryFullpage.min.css"  media="screen,projection"/>
        <link type="text/css" href="css/style.css" rel="stylesheet">
      <link type="text/css" rel="stylesheet" href="css/vectorial.css"  media="screen,projection"/>
      <link type="text/css" rel="stylesheet" href="css/interna.css"  media="screen,projection"/>
      <!--
      <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.1.20/jquery.fancybox.min.css"> -->
    </head>
    <body>

    <header>
        <nav class="cd-stretchy-nav">
            <a class="cd-nav-trigger" href="#0">
                Menu
                <span aria-hidden="true"></span>
            </a>

            <ul>
                <li><a href="fondo.php"><span>¿Qué es el Fondo?</span></a></li>
                <li><a href="convocatoria.php"><span>Convocatoria 2017</span></a></li>
                <li><a href="proyecto.php"><span>Postular mi proyecto</span></a></li>
                <li><a href="preguntas.php"><span>Preguntas frecuentes</span></a></li>
                <li><a href="contacto.php" class="active"><span>Contacto</span></a></li>
            </ul>

            <span aria-hidden="true" class="stretchy-nav-bg"></span>
        </nav>
    </header>

    <!-- Contenido -->
    <style>
        .flex.fullCenter.contentSectionBanner{
            background: url(img/Postule-su-proyecto-v2.jpg) center;
            background-size: cover;
            text-align: center;
            color: #fff;
        }
    </style>
    
    <div class="flex fullCenter menuContenedor">
        <img src="img/logo.png" alt="fondo fundación WWWB Colombia">
        <div class="flex menuItems">
            <ul class="flex menu">
                <li>
                    <a href="fondo.php">¿Qué es el Fondo?</a>
                </li>
                <li>
                    <a href="convocatoria.php">Convocatoria 2017</a>
                </li>
                <li>
                    <a href="proyecto.php">Postular mi proyecto</a>
                </li>
                <li>
                    <a href="preguntas.php">Preguntas frecuentes</a>
                </li>
                <li class="active">
                    <a href="contacto.php">Contacto</a>
                </li>
            </ul>
            <div class="flex fullCenter redesSociales">
                <a href="#">
                    <i class="fa fa-facebook" aria-hidden="true"></i>
                </a>
                <a href="#">
                    <i class="fa fa-twitter" aria-hidden="true"></i>
                </a>
                <a href="#">
                    <i class="fa fa-youtube-play" aria-hidden="true"></i>
                </a>
            </div>
        </div>
    </div>

   
  
  <div id="fullpage">

      <div class="section " id="section0">
          <div class="flex fullCenter contentSectionBanner">
              <div class="formulario">
               <h2>Contacto</h2>
               <h1>Lorem ipsum dolor sit amet consectetur adipisicing elit. Facilis, earum. Laborum, recusandae. 
                   Repudiandae similique dolorum obcaecati repellendus quo excepturi sequi officiis eos laudantium accusamus. 
                   Ut consequuntur provident odit perspiciatis nemo.</h1>
              </div>
              <form class="flex">
                  <input type="text" placeholder="Nombres y apellidos">
                  <input type="text" placeholder="Nombres y apellidos">
                  <input type="text" placeholder="Nombres y apellidos">
                  <input type="text" placeholder="Nombres y apellidos">
                  <input type="submit" value="Enviar">
              </form>
          </div>
      </div>

      
      <div class="section fp-auto-height azul" id="section2">
        <footer>
            <div class="flex contentFooter">
                <div class="col2">
                    <h3>Fondo Fundación WWB Colombia para la Investigación</h3>
                    <div class="flex menuFooter">
                        <div class="col1">
                            <ul>
                                <li>¿Qué es el Fondo?</li>
                                <li>¿Por qué lo hacemos?</li>
                                <li>¿A quién está dirigido?</li>
                                <li>¿Dónde aplica?</li>
                            </ul>
                        </div>
                        <div class="col1">
                            <ul>
                                <li>Postular mi proyecto</li>
                                <li>Financiamiento</li>
                                <li>Términos y condiciones</li>
                            </ul>
                        </div>
                        <div class="col1">
                            <ul>
                                <li>Preguntas frecuentes</li>
                                <li>Contáctenos</li>
                                <li>Conozca a la Fundación WWB Colombia</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col1">
                    <img src="img/logo.png">
                </div>
            </div>
            <div class="vectorial azulDestacado">
                <img src="img/vectorial.png">
            </div>
            <a class="anclaFoot" data-menuanchor="firstPage" href="#firstPage">
                <i class="fa fa-angle-up flechaArrow" aria-hidden="true"></i>
            </a>
        </footer>
      </div>

  </div>
  
  <div id="infoMenu">
      
  
  
  
  </div>
        

    <!-- /Contenido -->
    <!--Import jQuery before materialize.js-->
    <script
    src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
                                                      crossorigin="anonymous"></script>
    <script type="text/javascript" src="js/jquery.fullPage.js"></script>
    <script type="text/javascript" src="js/jquery.cycle.all.js"></script>

    <script type="text/javascript" src="https://cdn.carbonads.com/carbon.js?zoneid=1673&serve=C6AILKT&placement=codyhouseco"></script>
    <script src="https://codyhouse.co/demo/stretchy-navigation/js/main.js"></script>
    <!--
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.1.20/jquery.fancybox.min.js"></script> -->
    <script>
        $(document).ready(function(){
            
            $('#fullpage').fullpage({
              anchors: ['firstPage', 'secondPage', '3rdPage', '4thpage', 'lastPage'],
              menu: '#menu',
              'navigation': true,
			  'navigationPosition': 'right',
              responsiveWidth: 1200
            });
            $('a.ancla').click(function(e){
              e.preventDefault();
              var strAncla=$(this).attr('href');
                $('body,html').stop(true,true).animate({
                  scrollTop: $(strAncla).offset().top
                },1000);

            });
        });
      </script>
    </body>
  </html>
